const express = require('express')
const Sale = require('../models/sale')
const Car = require('../models/car')
const User = require('../models/user')
const mongoose = require('mongoose')
const auth = require('../middleware/auth')
const admin = require('../middleware/admin')
const router = express.Router()

router.get('/', [auth, admin], async(req, res) => {
  const sales = await Sale.find()
  res.send(sales)
})

router.post('/', auth, async(req, res) => {
  const user = await User.findById(req.body.userId)
  if(!user)
    return res.status(400).send('El usuario no existe')
  
  const car = await Car.findById(req.body.carId)
    if(!car)
      return res.status(400).send('El coche no existe')

  if(car.sold)
    return res.status(400).send('El coche ya fue vendido')

  const sale = new Sale({
    user: {
      _id: user._id,
      name: user.name,
      email: user.email
    },
    car: {
      _id: car._id,
      model: car.model
    },
    price: req.body.price
  })

  /*
  user.isCustomer = true
  user.save()

  car.sold = true
  car.save()

  const result = await sale.save()
  res.status(201).send(result)
  */

  /**
   * TRANSACTION. Se utiliza para asegurar que se cumplan todos las operaciones de ejecución
   * sí una falla las demás se suspenden
   */
  const session = await mongoose.startSession()
  session.startTransaction()
  try{    
    const result = await sale.save()
    
    user.isCustomer = true
    user.save()  
    car.sold = true
    car.save()

    await session.commitTransaction()
    session.endSession()
    res.status(201).send(result)
  } catch(e) {
    await session.abortTransaction()
    session.endSession()
    res.status(500).send(e.message)
  }

})

module.exports = router
