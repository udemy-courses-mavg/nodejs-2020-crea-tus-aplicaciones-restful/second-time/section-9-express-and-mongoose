const express = require('express')
const bcrypt = require('bcryptjs')
const User = require('../models/user')
const router = express.Router()
const { check, validationResult } = require('express-validator');

// ------------------------------------------------------------ POST ------------------------------------------------------------ 
router.post('/', [
  check('email').isLength({min: 2}),
  check('password').isLength({min: 3})
],
async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty())
    return res.status(422).json({ errors: errors.array() });

  let user = await User.findOne({email: req.body.email})
  if(!user)
    return res.status(400).send('Usuario o contraseña incorrectos')

  const validPassword = await bcrypt.compare(req.body.password, user.password)
  if(!validPassword)
    return res.status(400).send('Usuario o contraseña incorrectos')

  const jwtToken = user.generateJWT()
  // const jwtToken = jwt.sign({_id: user._id, name: user.name}, 'password_clavesita_pequeñita')
  
  // res.send('Usuario y contraseña correcta')
  // res.send(jwtToken)
  res.status(201).header('Authorization', jwtToken).send("")

  
})
module.exports = router


// https://github.com/Automattic/mongoose/issues/6890 =====================================