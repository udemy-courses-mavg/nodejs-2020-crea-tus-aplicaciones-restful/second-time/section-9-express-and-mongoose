const express = require('express')
const mongoose = require('mongoose')
const car = require('./routes/car')
const user = require('./routes/user')
const company = require('./routes/company')
const sale = require('./routes/sale')
const auth = require('./routes/auth')

const port = process.env.PORT || 3002
const app = express()
app.use(express.json())
app.use('/api/cars', car)
app.use('/api/users', user)
app.use('/api/companies', company)
app.use('/api/sales', sale)
app.use('/api/auth', auth)

console.log("SECRET KEY", process.env.SECRET_KEY_JWT_CAR_API);


app.listen(port, () => console.log("Escuchando en el puerto:", port))

mongoose.connect('mongodb://localhost/carsdb', {useNewUrlParser:true, useUnifiedTopology: true, useCreateIndex: true,})
  .then(() => console.log('Conectando a MongoDB'))