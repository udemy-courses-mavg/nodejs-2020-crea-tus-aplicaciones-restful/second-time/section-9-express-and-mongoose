const mongoose = require('mongoose')

const salesSchema = new mongoose.Schema({
  user: {
    // Se declara el 'new mongoose.Schema' dentro del user porque no se exportara a ningún lado, y solo se usará en 'Sales'
    type: new mongoose.Schema({
      name: String,
      email: String
    }),
    required: true
  },
  car: {
    type: new mongoose.Schema({
      model: String
    }),
    required: true
  },
  price: Number,
  date: {
    type: Date,
    default: Date.now
  }
})

const Sale = mongoose.model('sale', salesSchema)

module.exports = Sale